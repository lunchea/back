class TypeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message] || "#{value.class} is not of class #{options[:with]}") unless
      value.is_a? options[:with]
  end
end
