module Confirmable
  extend ActiveSupport::Concern

  included do
    scope :confirmed, -> { where.not(confirmed_at: nil) }
    scope :unconfirmed, -> { where(confirmed_at: nil) }

    def confirm!
      if !confirmed?
        update confirmed_at: Time.zone.now
      else
        true # prevent user from confirming multiple times
      end
    end

    def cancel!
      if confirmed?
        update confirmed_at: nil
      else
        true # prevent user from confirming multiple times
      end
    end

    def confirmed?
      confirmed_at != nil
    end
  end
end
