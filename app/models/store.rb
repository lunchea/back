class Store < ActiveRecord::Base
  include Confirmable
  searchkick
  resourcify # add role for stores
  reverse_geocoded_by :coordinate, address: :full_address
  geocoded_by :full_address
  monetize :min_delivery_sale_cents, allow_nil: true

  has_many :boxes, autosave: true
  has_many :orders
  belongs_to :creator, class_name: 'User'
  has_many :authentications, as: :authenticatable

  validates :name, presence: true
  validates :creator, presence: true
  after_save :set_creator_to_admin

  # after_validation :geocode
  # after_validation :reverse_geocode, if: :has_coordinates

  def full_address
    [street, city, state, country].compact.join(', ')
  end

  def set_creator_to_admin
    creator.add_role :admin, self
  end

  def dwolla_id
    authentications.find_by(provider: 'Dwolla').uid
  end
end
