class DeliveryItem < ActiveRecord::Base
  searchkick
  monetize :price_cents, allow_nil: true
  belongs_to :delivery
  belongs_to :chargable, polymorphic: true
  validates :quantity, numericality: { only_integer: true }
  before_create :set_price

  def set_price
    self.price = chargable.price * quantity
  end
end
