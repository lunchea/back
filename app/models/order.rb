class Order < ActiveRecord::Base
  searchkick
  belongs_to :creator, class_name: 'User'
  belongs_to :store, counter_cache: true
  has_many :deliveries
  enum status: [:pending, :processing, :delivering, :completed, :cancelled]
  validates :creator, presence: true
  validates :store, presence: true
  validates :delivered_at, presence: true
  validates :delivered_at, type: Time
  validate :enough_processing_time, on: :create
  after_create :schedule_notification, on: :create

  def schedule_notification
    OrderMailer.notify_store(self).deliver_later \
      wait_until: delivered_at - store.processing_time.seconds
  end

  def enough_processing_time
    Time.current < delivered_at - store.processing_time.seconds - 1.minute
  end

  def price
    deliveries.sum { |d| d.price || Money.new(0, 'USD') }
  end

  def accept!
    if status == 'pending'
      OrderMailer.send_summary_to_store(self).deliver_now
      ActiveRecord::Base.transaction do
        update accepted_at: Time.current, status: 'processing'
        deliveries.update_all status: 1
      end
    else
      errors.add :status, 'order has already been #{status}'
    end
  end

  def start_delivery!
    if status == 'processing'
      ActiveRecord::Base.transaction do
        update accepted_at: Time.current, status: 'delivering'
        deliveries.update_all status: 2
      end
    else
      errors.add :status, 'order has already been #{status}'
    end
  end
end
