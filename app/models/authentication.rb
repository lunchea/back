class Authentication < ActiveRecord::Base
  belongs_to :authenticatable, polymorphic: true

  def parse_code(redirect_uri, code)
    case provider
    when 'facebook'
      parse_code_facebook redirect_uri, code
    else
      fail 'unsupported provider'
    end
  end

  def parse_code_facebook(redirect_uri, code)
    self.token = Oauth::KOALA_FACEBOOK.get_access_token \
      code,
      redirect_uri: redirect_uri
    @graph = Koala::Facebook::API.new token
    self.profile = @graph.get_object('me')
    self.uid = profile['id']
  end

  def create_authenticatable
    user = User.new email: profile['email']
    user.save validate: false
    self.authenticatable = user
  end

  def get_existing_one_or_associated_with(current_user)
    existing_one = self.class.find_by \
      uid: uid,
      provider: provider,
      authenticatable_type: authenticatable_type
    return existing_one if existing_one
    self.authenticatable = current_user
    create_authenticatable unless authenticatable
    self
  end

  def self.get_auth_url(provider, redirect_uri)
    case provider
    when 'facebook'
      Oauth::KOALA_FACEBOOK.url_for_oauth_code redirect_uri: redirect_uri
    else
      fail 'unsupported provider'
    end
  end
end
