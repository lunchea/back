class Delivery < ActiveRecord::Base
  searchkick
  monetize :price_cents, allow_nil: true
  enum status: [:pending, :processing, :delivering, :delivered]
  belongs_to :order
  belongs_to :user
  has_many :items, class_name: 'DeliveryItem'
  accepts_nested_attributes_for :items
  validate :order_still_pending, on: :create
  validates :order, presence: true
  validates :user, presence: true
  validates :items, presence: true

  def order_still_pending
    return true unless order
    errors.add :order_id, 'order is not in the pending status'  unless
      order.status == 'pending'
  end
end
