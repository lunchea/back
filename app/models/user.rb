class User < ActiveRecord::Base
  include Confirmable
  has_secure_password
  rolify
  searchkick
  has_many :authentications, as: :authenticatable
  has_many :stores, foreign_key: 'creator_id'

  validates :email, uniqueness: true, email: true
  validates :username, uniqueness: true, allow_nil: true
  before_create :set_avatar

  def set_avatar
    return true if avatar_url
    self.avatar_url = 'https://gravatar.com/avatar/'
    self.avatar_url += Digest::MD5.hexdigest(email.to_s.downcase)
  end

  def reset_password!(new_password)
    self.password = new_password
    save
  end

  def self.find_by_pw_reset_jwt(token)
    find_by_jwt token
  end

  def self.find_by_confirmation_jwt(token)
    find_by_jwt token
  end

  def self.find_by_jwt(token)
    payloader = decode_jwt token
    find payloader[0]['id']
  rescue JWT::DecodeError
    false
  end

  def self.encode_jwt(hash)
    secret = Rails.application.secrets['secret_key_base']
    JWT.encode(hash, secret, 'HS384')
  end

  def self.decode_jwt(token)
    secret = Rails.application.secrets['secret_key_base']
    JWT.decode(token, secret, 'HS384')
  end

  def pay_load
    {
      id: id,
      email: email,
      exp: (Time.zone.now + 1.week).to_i
    }
  end
end
