class Box < ActiveRecord::Base
  searchkick
  monetize :price_cents, allow_nil: true
  belongs_to :store
  has_many :delivery_items, as: :chargable

  validates :store, presence: true
end
