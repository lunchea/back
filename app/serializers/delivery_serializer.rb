class DeliverySerializer < ActiveModel::Serializer
  attributes :id, :order_id, :delivered_at, :contact_info, :status, :price

  has_many :items
end
