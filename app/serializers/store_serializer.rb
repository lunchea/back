class StoreSerializer < ActiveModel::Serializer
  attributes :id, :name, :confirmed_at, :min_delivery_sale, :processing_time,
             :contact_info, :pending_orders_count, :authentications
  has_many :boxes
  url :store
end
