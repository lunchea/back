class OrderSerializer < ActiveModel::Serializer
  attributes :id, :creator_id, :store_id, :status, :delivered_at, :accepted_at

  has_many :deliveries
end
