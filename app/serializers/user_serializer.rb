class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :avatar_url, :profile, :confirmed_at, :errors
  has_many :roles
  url :user

  def attributes(*args)
    data = super
    data[:token] = User.encode_jwt object.pay_load if @options[:token]
    data
  end
end
