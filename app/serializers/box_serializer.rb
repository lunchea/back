class BoxSerializer < ActiveModel::Serializer
  attributes :id, :name, :details, :store_id, :price
end
