class DeliveryItemSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :chargable_id, :chargable_type, :chargable, :price
end
