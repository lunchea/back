class NotificationsMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifications_mailer.forgot_password.subject
  #
  def forgot_password(user, callback_url, token_key)
    @user = user
    token = User.encode_jwt @user.pay_load
    @callback_url = callback_url + '?' + token_key + '=' + token
    mail to: user.email,
         subject: 'Lunchea.com - Forgot your password?'
  end

  def confirm_email(user, callback_url, token_key)
    @user = user
    token = User.encode_jwt @user.pay_load
    @callback_url = callback_url + '?' + token_key + '=' + token
    mail to: @user.email,
         subject: 'Lunchea.com - Please confirm your email'
  end
end
