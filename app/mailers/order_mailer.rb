class OrderMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.notify_store.subject
  #
  def notify_store(order)
    @order = order
    mail to: @order.store.contact_info['email'],
         subject: 'Lunchea.com - A new order pending, want to accept it?'
  end

  def send_summary_to_store(order)
    @order = order
    mail to: @order.store.contact_info['email'],
         subject: 'Lunchea.com - A new order summary'
  end
end
