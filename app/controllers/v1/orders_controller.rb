class V1::OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  # GET /orders
  def index
    @orders = policy_scope Order

    render json: @orders
  end

  # GET /orders/1
  def show
    render json: @order
  end

  # POST /orders
  def create
    @order = Order.new(order_params)
    authorize @order

    if @order.save
      render json: @order, status: :created, location: v1_order_url(@order)
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
    authorize @order
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit \
      :store_id, :creator_id, :delivered_at
  end
end
