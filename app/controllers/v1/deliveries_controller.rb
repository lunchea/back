class V1::DeliveriesController < ApplicationController
  before_action :set_delivery, only: [:show, :update, :destroy]
  # after_action :verify_authorized, except: :index
  # after_action :verify_policy_scoped, only: :index

  # GET /deliveries
  def index
    @deliveries = policy_scope Delivery

    render json: @deliveries
  end

  # GET /deliveries/1
  def show
    render json: @delivery
  end

  # POST /deliveries
  def create
    @delivery = Delivery.new(delivery_params)
    authorize @delivery

    if @delivery.save
      render json: @delivery, status: :created,
             location: v1_delivery_url(@delivery)
    else
      render json: @delivery.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /deliveries/1
  def update
    if @delivery.update(delivery_params)
      render json: @delivery
    else
      render json: @delivery.errors, status: :unprocessable_entity
    end
  end

  # DELETE /deliveries/1
  def destroy
    @delivery.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_delivery
    @delivery = Delivery.find(params[:id])
    authorize @delivery
  end

  # Only allow a trusted parameter "white list" through.
  def delivery_params
    params.require(:delivery).permit :user_id, items_attributes: [:quantity]
  end
end
