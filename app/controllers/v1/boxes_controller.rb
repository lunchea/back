class V1::BoxesController < ApplicationController
  before_action :set_box, only: [:show, :update, :destroy]
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  # GET /boxes
  def index
    @boxes = policy_scope Box

    render json: @boxes
  end

  # GET /boxes/1
  def show
    render json: @box
  end

  # POST /boxes
  def create
    @box = Box.new(box_params)
    authorize @box

    if @box.save
      render json: @box, status: :created, location: v1_box_url(@box)
    else
      render json: @box.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /boxes/1
  def update
    if @box.update(box_params)
      render json: @box
    else
      render json: @box.errors, status: :unprocessable_entity
    end
  end

  # DELETE /boxes/1
  def destroy
    @box.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_box
    @box = Box.find(params[:id])
    authorize @box
  end

  # Only allow a trusted parameter "white list" through.
  def box_params
    params.require(:box).permit \
      :name, :price, :store_id,
      details: [:chinese_name, :category, :desc]
  end
end
