class V1::StoresController < ApplicationController
  before_action :set_store, only: [:show, :update, :destroy, :confirm, :cancel]
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  # GET /stores
  def index
    @stores = policy_scope Store

    render json: @stores
  end

  # GET /stores/1
  def show
    render json: @store
  end

  # POST /stores
  def create
    @store = Store.new(store_params)
    authorize @store

    if @store.save
      render json: @store, status: :created, location: v1_store_url(@store)
    else
      render json: @store.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /stores/1
  def update
    if @store.update(store_params)
      render json: @store
    else
      render json: @store.errors, status: :unprocessable_entity
    end
  end

  # DELETE /stores/1
  def destroy
    @store.destroy
  end

  def confirm
    if @store.confirm!
      render json: @store
    else
      render json: @store, status: :unprocessable_entity
    end
  end

  def cancel
    if @store.cancel!
      render json: @store
    else
      render json: @store, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_store
    @store = Store.find(params[:id])
    authorize @store
  end

  # Only allow a trusted parameter "white list" through.
  def store_params
    params.require(:store).permit \
      :name, :processing_time, :min_delivery_sale, :contact_info
  end
end
