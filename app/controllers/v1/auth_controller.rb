class V1::AuthController < ApplicationController
  before_action :set_redirect_uri, only: [:oauth_auth_url, :oauth_callback]

  def login
    @user = User.find_by email: params[:email]
    if @user.authenticate params[:password]
      render json: @user, token: true
    else
      render json: @user.errors, status: :unauthorized
    end
  end

  def signup
    @user = User.new email: params[:email], password: params[:password]
    if @user.save
      render json: @user, token: true
    else
      render json: @user.errors, status: :unauthorized
    end
  end

  def request_reset
    @user = User.find_by email: params[:email]
    head 404 unless @user && @user.valid?
    NotificationsMailer
      .forgot_password(@user, params[:callback_url], params[:token_key])
      .deliver_later
  end

  def reset
    @user = User.find_by_pw_reset_jwt params[:reset_password_token]
    head 404 unless @user && @user.valid?
    head 500 unless @user.reset_password! params[:password]
  end

  def request_confirm
    @user = User.find params[:id]
    head 404 unless @user && @user.valid?
    NotificationsMailer
      .confirm_email(@user, params[:callback_url], params[:token_key])
      .deliver_later
  end

  def confirm
    @user = User.find_by_confirmation_jwt params[:confirmation_token]
    head 404 unless @user && @user.valid?
    if @user.confirm!
      render json: @user, token: true
    else
      head 422
    end
  end

  def oauth_auth_url
    auth_url = Authentication.get_auth_url params['provider'],
                                           params['redirect_uri']
    redirect_to auth_url
  end

  def oauth_callback
    auth = Authentication.new \
      provider: params['provider'],
      authenticatable_type: params['authenticatable_type']
    auth.parse_code params['redirect_uri'],
                    params['code']

    auth = auth.get_existing_one_or_associated_with current_user
    render json: auth.authenticatable, token: true
  end

  private

  # Only allow a trusted parameter "white list" through.
  def set_redirect_uri
    params['redirect_uri'] << '/'  if params['redirect_uri'][-1] != '/'
  end
end
