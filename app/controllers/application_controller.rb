class ApplicationController < ActionController::API
  include Pundit

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from Pundit::NotAuthorizedError, with: :not_authorized

  private

  def record_not_found(e)
    render json: e.to_json, status: 404
  end

  def not_authorized(e)
    render json: e.to_json, status: 401
  end

  def current_user
    @current_user ||= User.find_by_jwt request.headers['X-Jwt']
    @current_user
  rescue JWT::DecodeError
    false
  end
end
