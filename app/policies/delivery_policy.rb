class DeliveryPolicy < ApplicationPolicy
  def create?
    return false unless user.is_a? User
    record.user_id = user.id
  end

  def update?
    true
  end

  def show?
    true
  end

  class Scope < Scope
    def resolve
      if !user.is_a? User
        scope.none
      elsif user.has_role? :admin
        scope.order(created_at: :desc)
      else
        scope.order(created_at: :desc)
      end
    end
  end
end
