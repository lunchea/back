class StorePolicy < ApplicationPolicy
  def create?
    return false unless user.is_a? User
    return true if user.has_role?(:admin)
    record.creator_id = user.id
  end

  def update?
    return false unless user.is_a? User
    return true if user.has_role? :admin
    user.has_role? :admin, record
  end

  def show?
    record.persisted?
  end

  def confirm?
    user.is_a?(User) && user.has_role?(:admin)
  end

  def destroy?
    user.is_a?(User) && user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope.order created_at: :desc
    end
  end
end
