class OrderPolicy < ApplicationPolicy
  def create?
    return false unless user.is_a? User
    record.creator_id = user.id
  end

  def update?
    true
  end

  def show?
    true
  end

  def accept?
    return false unless user.is_a? User
    return true if user.has_role? :admin
    user.has_role? :admin, record.store
  end

  def destroy?
    return false unless user.is_a? User
    user.has_role? :admin
  end

  class Scope < Scope
    def resolve
      return scope.where.not(status: 4).order(delivered_at: :desc) unless
        user.is_a? User

      unless user.has_role? :admin
        scoped = scope.where 'status <= :status OR creator_id = :creator_id',
                             status: 3,
                             creator_id: user.id
      end
      (scoped || scope).order delivered_at: :desc
    end
  end
end
