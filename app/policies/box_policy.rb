class BoxPolicy < ApplicationPolicy
  def create?
    user.is_a?(User) && (user.has_role?(:admin) ||
      user.has_role?(:admin, record.store))
  end

  def update?
    user.is_a?(User) && (user.has_role?(:admin) ||
      user.has_role?(:admin, record.store))
  end

  def destroy?
    user.is_a?(User) && (user.has_role?(:admin) ||
      user.has_role?(:admin, record.store))
  end

  class Scope < Scope
    def resolve
      scope.order(created_at: :desc)
    end
  end
end
