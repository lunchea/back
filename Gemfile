source 'https://rubygems.org'

gem 'rails', github: "rails/rails"
gem 'arel', github: "rails/arel"
gem 'rack', github: "rack/rack"

# Use sqlite3 as the database for Active Record
gem 'pg'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'puma'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use ActiveModelSerializers to serialize JSON responses
gem 'active_model_serializers', '~> 0.10.0.rc2'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'annotate'
end

# authentication
gem 'jwt'
gem 'koala'

# model logic
gem 'rolify'
gem 'pundit'
gem 'money'
gem 'money-rails'
gem 'geocoder'

# full text search
gem 'searchkick'

# # Background jobs
gem 'sidekiq'

# Paignator
gem 'kaminari'
