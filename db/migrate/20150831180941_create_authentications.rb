class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.belongs_to :authenticatable, null: false, polymorphic: true
      t.string :uid, null: false
      t.string :provider, null: false
      t.string :token
      t.string :secret
      t.string :refresh
      t.string :token_type
      t.json :profile

      t.timestamps null: false
    end
    add_index :authentications, :authenticatable_id
    add_index :authentications, :authenticatable_type
    add_index :authentications, :uid
    add_index :authentications, :provider
  end
end
