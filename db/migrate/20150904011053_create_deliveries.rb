class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.belongs_to :order, null: false
      t.belongs_to :user, null: false
      t.integer :status, null: false, default: 0
      t.monetize :price, amount: { null: true, default: 0 }
      t.json :contact_info, null: false, default: {}
      t.datetime :delivered_at, null: true
      t.json :details

      # geo info
      t.string :street
      t.string :city
      t.string :state
      t.string :country
      t.point :coordinates

      t.timestamps null: false
    end
  end
end
