class CreateBoxes < ActiveRecord::Migration
  def change
    create_table :boxes do |t|
      t.string :name, null: false
      t.belongs_to :store, null: false
      t.json :details
      t.string :image_url
      t.monetize :price, amount: { null: true, default: 0 }

      t.timestamps null: false
    end
  end
end
