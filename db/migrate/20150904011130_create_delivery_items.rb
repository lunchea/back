class CreateDeliveryItems < ActiveRecord::Migration
  def change
    create_table :delivery_items do |t|
      t.belongs_to :delivery, null: false
      t.references :chargable, polymorphic: true, index: true
      t.integer :quantity, null: false, default: 1
      t.monetize :price, amount: { null: true, default: 0 }
      t.json :details

      t.timestamps null: false
    end
  end
end
