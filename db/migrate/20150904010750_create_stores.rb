class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name, null: false
      t.json :contact_info
      t.monetize :min_delivery_sale, amount: { null: true, default: 0 }
      t.integer :processing_time, default: 1800, null: false # in seconds
      t.belongs_to :creator, null: false
      t.integer :orders_count, default: 0, null: false
      t.integer :pending_orders_count, default: 0, null: false
      t.integer :processing_orders_count, default: 0, null: false
      t.json :details

      # geo info
      t.string :street
      t.string :city
      t.string :state
      t.string :country
      t.point :coordinates
      t.decimal :delivery_range, scale: 3, precision: 15

      # confirmable
      t.datetime :confirmed_at

      t.timestamps null: false
    end
  end
end
