class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :store, null: false
      t.belongs_to :creator, null: false
      t.integer :status, null: false, default: 0
      t.json :details

      # geo info
      t.string :street
      t.string :city
      t.string :state
      t.string :country
      t.point :coordinates

      # timing
      t.datetime :accepted_at
      t.datetime :delivered_at, null: false

      t.timestamps null: false
    end

    add_index :orders, :delivered_at
  end
end
