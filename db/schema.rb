# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150904011130) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "authentications", force: :cascade do |t|
    t.string   "authenticatable_type", null: false
    t.integer  "authenticatable_id",   null: false
    t.string   "uid",                  null: false
    t.string   "provider",             null: false
    t.string   "token"
    t.string   "secret"
    t.string   "refresh"
    t.string   "token_type"
    t.json     "profile"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["authenticatable_id"], name: "index_authentications_on_authenticatable_id", using: :btree
    t.index ["authenticatable_type"], name: "index_authentications_on_authenticatable_type", using: :btree
    t.index ["provider"], name: "index_authentications_on_provider", using: :btree
    t.index ["uid"], name: "index_authentications_on_uid", using: :btree
  end

  create_table "boxes", force: :cascade do |t|
    t.string   "name",                           null: false
    t.integer  "store_id",                       null: false
    t.json     "details"
    t.string   "image_url"
    t.integer  "price_cents",    default: 0
    t.string   "price_currency", default: "USD", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "deliveries", force: :cascade do |t|
    t.integer  "order_id",                       null: false
    t.integer  "user_id",                        null: false
    t.integer  "status",         default: 0,     null: false
    t.integer  "price_cents",    default: 0
    t.string   "price_currency", default: "USD", null: false
    t.json     "contact_info",   default: {},    null: false
    t.datetime "delivered_at"
    t.json     "details"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.point    "coordinates"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "delivery_items", force: :cascade do |t|
    t.integer  "delivery_id",                    null: false
    t.string   "chargable_type"
    t.integer  "chargable_id"
    t.integer  "quantity",       default: 1,     null: false
    t.integer  "price_cents",    default: 0
    t.string   "price_currency", default: "USD", null: false
    t.json     "details"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["chargable_type", "chargable_id"], name: "index_delivery_items_on_chargable_type_and_chargable_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "store_id",                 null: false
    t.integer  "creator_id",               null: false
    t.integer  "status",       default: 0, null: false
    t.json     "details"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.point    "coordinates"
    t.datetime "accepted_at"
    t.datetime "delivered_at",             null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["delivered_at"], name: "index_orders_on_delivered_at", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name",                                                                null: false
    t.json     "contact_info"
    t.integer  "min_delivery_sale_cents",                             default: 0
    t.string   "min_delivery_sale_currency",                          default: "USD", null: false
    t.integer  "processing_time",                                     default: 1800,  null: false
    t.integer  "creator_id",                                                          null: false
    t.integer  "orders_count",                                        default: 0,     null: false
    t.integer  "pending_orders_count",                                default: 0,     null: false
    t.integer  "processing_orders_count",                             default: 0,     null: false
    t.json     "details"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.point    "coordinates"
    t.decimal  "delivery_range",             precision: 15, scale: 3
    t.datetime "confirmed_at"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "username"
    t.json     "profile",                default: {}
    t.string   "avatar_url"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

end
