require 'test_helper'

class OrderMailerTest < ActionMailer::TestCase
  test "notify_store" do
    mail = OrderMailer.notify_store
    assert_equal "Notify store", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
