require 'test_helper'

class V1::OrdersControllerTest < ActionController::TestCase
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post :create, params: { order: {  } }
    end

    assert_response 201
  end

  test "should show order" do
    get :show, params: { id: @order }
    assert_response :success
  end

  test "should update order" do
    patch :update, params: { id: @order, order: {  } }
    assert_response 200
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete :destroy, params: { id: @order }
    end

    assert_response 204
  end
end
