require 'test_helper'

class V1::StoresControllerTest < ActionController::TestCase
  setup do
    @store = stores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should create store" do
    assert_difference('Store.count') do
      post :create, params: { store: {  } }
    end

    assert_response 201
  end

  test "should show store" do
    get :show, params: { id: @store }
    assert_response :success
  end

  test "should update store" do
    patch :update, params: { id: @store, store: {  } }
    assert_response 200
  end

  test "should destroy store" do
    assert_difference('Store.count', -1) do
      delete :destroy, params: { id: @store }
    end

    assert_response 204
  end
end
