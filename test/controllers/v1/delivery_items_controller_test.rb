require 'test_helper'

class V1::DeliveryItemsControllerTest < ActionController::TestCase
  setup do
    @delivery_item = delivery_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should create delivery_item" do
    assert_difference('DeliveryItem.count') do
      post :create, params: { delivery_item: {  } }
    end

    assert_response 201
  end

  test "should show delivery_item" do
    get :show, params: { id: @delivery_item }
    assert_response :success
  end

  test "should update delivery_item" do
    patch :update, params: { id: @delivery_item, delivery_item: {  } }
    assert_response 200
  end

  test "should destroy delivery_item" do
    assert_difference('DeliveryItem.count', -1) do
      delete :destroy, params: { id: @delivery_item }
    end

    assert_response 204
  end
end
