require 'test_helper'

class V1::DeliveriesControllerTest < ActionController::TestCase
  setup do
    @delivery = deliveries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should create delivery" do
    assert_difference('Delivery.count') do
      post :create, params: { delivery: {  } }
    end

    assert_response 201
  end

  test "should show delivery" do
    get :show, params: { id: @delivery }
    assert_response :success
  end

  test "should update delivery" do
    patch :update, params: { id: @delivery, delivery: {  } }
    assert_response 200
  end

  test "should destroy delivery" do
    assert_difference('Delivery.count', -1) do
      delete :destroy, params: { id: @delivery }
    end

    assert_response 204
  end
end
