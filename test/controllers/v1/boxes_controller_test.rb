require 'test_helper'

class V1::BoxesControllerTest < ActionController::TestCase
  setup do
    @box = boxes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should create box" do
    assert_difference('Box.count') do
      post :create, params: { box: {  } }
    end

    assert_response 201
  end

  test "should show box" do
    get :show, params: { id: @box }
    assert_response :success
  end

  test "should update box" do
    patch :update, params: { id: @box, box: {  } }
    assert_response 200
  end

  test "should destroy box" do
    assert_difference('Box.count', -1) do
      delete :destroy, params: { id: @box }
    end

    assert_response 204
  end
end
