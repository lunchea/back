module Oauth
  KOALA_FACEBOOK = Koala::Facebook::OAuth.new(
    Rails.application.secrets['facebook_app_id'],
    Rails.application.secrets['facebook_app_secret'])
end
