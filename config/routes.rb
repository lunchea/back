Rails.application.routes.draw do
  namespace :v1 do
    resources :delivery_items
    resources :deliveries
    resources :orders
    resources :boxes
    resources :stores
    resources :users
    post 'auth/login'
    post 'auth/signup'
    post 'auth/request_reset'
    post 'auth/request_confirm'
    put  'auth/reset'
    put  'auth/confirm'
    get  'auth/oauth_auth_url'
    post 'auth/oauth_callback'
  end
  root to: 'v1/users#index'
end
